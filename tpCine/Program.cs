﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Globalization;

namespace tpCine
{
    class Program
    {
        static void Main(string[] args)
        {
            Cinema cine = new Cinema("ugc");

            Salle uneSalle = new Salle("h", 1, 50);
            Salle uneSalleDeux = new Salle("b", 2, 100);
            Salle uneSalleTrois = new Salle("c", 3, 150);

            cine.AjouterSalle(uneSalle);
            cine.AjouterSalle(uneSalleDeux);
            cine.AjouterSalle(uneSalleTrois);


            foreach(Salle cinemaCourant in cine.CollectionSalle)
            {
                Console.WriteLine(cinemaCourant.Capacite);
                Console.WriteLine(cinemaCourant.NumSalle);
                Console.WriteLine(cinemaCourant.NomDeLaSalle);
            }

            float capacite = cine.GetCapaciteTotalDuCinema();
            Console.WriteLine("Capacite total du cinema : {0}",capacite);

            Salle salleLaP = cine.GetLaSalleLaPlusGrande();
            Console.WriteLine("La salle la plus grande est {0}",salleLaP.NomDeLaSalle);

            bool g = cine.SuprimmerSalle(2);
            Console.WriteLine(g);


            foreach (Salle cinemaCourant in cine.CollectionSalle)
            {
                Console.WriteLine(cinemaCourant.Capacite);
                Console.WriteLine(cinemaCourant.NumSalle);
                Console.WriteLine(cinemaCourant.NomDeLaSalle);
            }


        }
    }
}
