﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace tpCine
{
    class Cinema
    {
        private string _nomDuCinema;
        private List<Salle> _collectionSalle = new List<Salle>();
        public Cinema(string nomDuCinema)
        {
            _nomDuCinema = nomDuCinema;
        }

        public string NomDuCinema { get => _nomDuCinema; set => _nomDuCinema = value; }
        internal List<Salle> CollectionSalle { get => _collectionSalle; }

        public bool ExisteDansLesSallesDuCinema(Salle uneSalle)
        {
            bool existe = false;
         

            foreach(Salle salleCourant in _collectionSalle)
            {
                if(salleCourant.NomDeLaSalle == uneSalle.NomDeLaSalle && salleCourant.NumSalle == uneSalle.NumSalle)
                {
                    existe = true;
                    break;
                }
            }
            return existe;
        }


        public Salle GetSalle(int numSalle)
        {
            Salle salle = null;

            foreach (Salle salleCourant in _collectionSalle)
            {
                if (salleCourant.NumSalle == numSalle)
                {
                    salle = salleCourant;
                    break;
                }
            }

            return salle;
        }

        public void AjouterSalle(Salle uneSalle)
        {
            //bool AjtSal = false;

            //foreach (Salle salleCourant in _collectionSalle)
            //{
            //    if(salleCourant == uneSalle)
            //    {
            //        AjtSal = true;
            //        break;
            //    }
            //}

            //if(AjtSal == false)
            //{
            //    _collectionSalle.Add(uneSalle);
            //}


            if (_collectionSalle.Exists(s => s == uneSalle) == false)
            {
                _collectionSalle.Add(uneSalle);
            }



        }

        public bool SuprimmerSalle(int numSalle)
        {
            bool suprimmer = false;


            foreach (Salle salleCourant in _collectionSalle)
            {
                if (salleCourant.NumSalle == numSalle)
                {
                    _collectionSalle.Remove(salleCourant);

                    suprimmer = true;
                    break;
                }
            }

        

            return suprimmer;
        }

        public float GetCapaciteTotalDuCinema()
        {
            float capacite = 0;

            foreach (Salle salleCourant in _collectionSalle)
            {
                capacite += salleCourant.Capacite;
            }

            return capacite;
        }

        public Salle GetLaSalleLaPlusGrande()
        {
            
            Salle salle = _collectionSalle[0];

            foreach(Salle salleCourant in _collectionSalle)
            {
                if(salleCourant.Capacite > salle.Capacite)
                {
                    salle = salleCourant;
                }
            }

            return salle;

        }




    }
}
