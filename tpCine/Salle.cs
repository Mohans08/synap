﻿using System;
using System.Collections.Generic;
using System.Text;

namespace tpCine
{
    class Salle
    {
        private string _nomDeLaSalle;
        private int _numSalle;
        private int _capacite;

        public Salle(string nomDeLaSalle, int numSalle, int capacite)
        {
            _nomDeLaSalle = nomDeLaSalle;
            _numSalle = numSalle;
            _capacite = capacite;
        }


        public int NumSalle { get => _numSalle;  }
        public int Capacite { get => _capacite; }
        public string NomDeLaSalle { get => _nomDeLaSalle; set => _nomDeLaSalle = value; }

        public bool MemeSalle(Salle uneSalle)
        {
            bool meme = false;
            if(_nomDeLaSalle == uneSalle._nomDeLaSalle && _numSalle == uneSalle._numSalle)
            {
                meme = true;
            }

            return meme;
        }
    }
}
