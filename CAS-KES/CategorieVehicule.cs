﻿using System;

namespace CasKes
{
    [Serializable]
    class CategorieVehicule
    {
        private string _categorieVoiture;
        private float _tarifKilometrique;

        public  CategorieVehicule(string categorieVehicule, float coeficientApplique)
        {
            _categorieVoiture = categorieVehicule;
            _tarifKilometrique = coeficientApplique;
        }

        public float TarifKilometrique { get => _tarifKilometrique; set => _tarifKilometrique = value; }
        
    }
}
