﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CasKes
{
    class Program
    {
        
        static void Main(string[] args)
        {
            #region CHARGEMENT DES DONNEES
            List<Grade> lesGrades = new List<Grade>();
            lesGrades.Add(new Grade("Technicien Niveau 1", 10));
            lesGrades.Add(new Grade("Technicien Niveau 2", 11));
            lesGrades.Add(new Grade("Technicien Superieur Niveau 1", 12.5f));
            lesGrades.Add(new Grade("Technicien Superieur Niveau 2", 14f));
            lesGrades.Add(new Grade("Expert", 18));


            List<CategorieVehicule> lescategorieVehicule = new List<CategorieVehicule>();

            List<Client> lesClients = Persistance.ChargerClients();
            if (lesClients == null)
            {
                lesClients = new List<Client>();//Creation d'une collection de clients + jeu d'essai
                lesClients.Add(new Client(1, "SuperMag", "1 rue de la Grande Armee", "75015", "Paris", 18));
                lesClients.Add(new Client(2, "AuMag", "12 rue du Chêne", "95500", "Cergy", 34));
                lesClients.Add(new Client(3, "SuperMarch", "21 rue du Sapin", "78700", "Conflans", 56));
                lesClients.Add(new Client(4, "MagBio", "33 rue du Bouleau", "95680", "Enghien les bains", 29));

            }

            List<Employe> lesEmployes = Persistance.ChargerEmployes();
            if(lesEmployes == null)
            {
                lesEmployes = new List<Employe>();
                lesEmployes.Add(new Employe(1, "Dupont", lesGrades[0], new DateTime(2005, 10, 21)));
                lesEmployes.Add(new Employe(2, "Durant", lesGrades[1], new DateTime(2010, 9, 12)));
                lesEmployes.Add(new Employe(3, "Duverne", lesGrades[2], new DateTime(2015, 4, 5)));
                lesEmployes.Add(new Employe(4, "Clovis", lesGrades[3], new DateTime(2012, 8, 7)));
            }
            #endregion

            //int numEmploye = 1;
            //string nomEmploye = "Marc";
            //Grade grade = new Grade("Technicien Niveau 1", 14);
            //DateTime dateEmbauche = new DateTime(2012, 10, 21);

            //Employe unEmploye = new Employe(numEmploye, nomEmploye, grade, dateEmbauche);

            //int numClient = 1;
            //string nomClient = "SuperMag";
            //string adresseClient = "1 rue de la Grande Armee";
            //string codePostal = "95500";
            //string ville = "Paris";
            //float nbKmEntreAgenceEtClient = 18;

            //Client unClient = new Client(numClient, nomClient, adresseClient, codePostal, ville, nbKmEntreAgenceEtClient);

            //unClient.AjouterContrat(2, 30, new DateTime(2005, 10, 21));


            bool fermerLeProgramme = false;
            do
            {
                #region MENU
                Console.WriteLine("1-LISTE CLIENTS");
                Console.WriteLine("2-LISTE EMPLOYES");
                Console.WriteLine("3- AJOUTER EMPLOYE");
                Console.WriteLine("4- AJOUTER CLIENT");
                Console.WriteLine("5- AJOUTER CONTRAT");
                Console.WriteLine("6- SUPPRIMER CONTRAT");
                Console.WriteLine("7- OBTENIR CONTRAT");
                Console.WriteLine("8- AJOUTER INTERVENTION");
                Console.WriteLine("0- SAVE AND EXIT");
                #endregion

                string chaineSaisie = Console.ReadLine();
                switch (chaineSaisie)
                {
                    case "0":
                      //  Persistance.SauvegarderClients(lesClients);
                        Persistance.SauvegarderEmployes(lesEmployes);
                        fermerLeProgramme = true;
                        break;

                    case "1":
                        foreach (Client c in lesClients)
                        {
                            Console.WriteLine(c.NomClient);
                        }
                        break;

                    case "2":
                        foreach (Employe e in lesEmployes)
                        {
                            Console.WriteLine(e.NomEmploye);
                        }
                        break;

                    case "3":
                        Console.WriteLine("NUM EMPLOYE ?");
                        int numEmploye = Convert.ToInt32(Console.ReadLine());
                        Console.WriteLine("NOM EMPLOYE ?");
                        string nomEmploye = Console.ReadLine();
                        Console.WriteLine("DATE EMBAUCHE ?");
                        string dateSaisie = Console.ReadLine();
                        DateTime dt = Convert.ToDateTime(dateSaisie);
                        Grade gradeEmploye = null;

                        bool saisieValide = false;
                        do
                        {
                            Console.WriteLine("GRADE");
                            string grade = Console.ReadLine();
                            foreach (Grade g in lesGrades)
                            {
                                if (g.NomGrade == grade)
                                {
                                    gradeEmploye = g;
                                    saisieValide = true;
                                }
                            }
                        } while (saisieValide == false);
                        Employe unEmploye = new Employe(numEmploye, nomEmploye, gradeEmploye, dt);
                        lesEmployes.Add(unEmploye);
                        break;
                    case "4":
                        Console.WriteLine("NUM CLIENT");
                        int numClient = Convert.ToInt32(Console.ReadLine());
                        Console.WriteLine("NOM CLIENT ?");
                        string nomClient = Console.ReadLine();
                        Console.WriteLine("ADRESSE CLIENT ?");
                        string adresseClient = Console.ReadLine();
                        Console.WriteLine("CODE POSTAL ?");
                        string codePostal = Console.ReadLine();
                        Console.WriteLine("VILLE ?");
                        string ville = Console.ReadLine();
                        Console.WriteLine("nbKmEntreAgenceEtClient ?");
                        float nbKmEntreAgenceEtClient = Convert.ToSingle(Console.ReadLine());

                        Client unClient = new Client(numClient, nomClient, adresseClient, codePostal, ville, nbKmEntreAgenceEtClient);
                        lesClients.Add(unClient);
                        break;
                    case "5":
                        foreach (Client c in lesClients)
                        {
                            Console.WriteLine(c.NomClient);
                            
                        }

                        bool saisie = false;
                        do
                        {
                            Console.WriteLine("SAISIR UN CLIENT ?");
                            string cli = Console.ReadLine();
                            foreach (Client c in lesClients)
                            {
                                if (c.NomClient == cli)
                                {                                 
                                    float montantContrat = 0;
                                    bool montant = false;
                                    do
                                    {
                                        Console.WriteLine("MONTANT CONTRAT ?");
                                        chaineSaisie = Console.ReadLine();

                                        if (float.TryParse(chaineSaisie, out montantContrat) == true)
                                        {
                                            montant = true;
                                        }
                                    } while (montant == false);
                                    DateTime date = DateTime.Now;
                                    c.AjouterContrat(montantContrat, date);
                                    saisie = true;
                                    break;

                                }

                                
                            }
                        } while (saisie == false);
                        break;

                    case "6":
                        foreach (Client c in lesClients)
                        {
                            Console.WriteLine(c.NomClient);

                        }


                        bool saisieC = false;
                        do
                        {
                            Console.WriteLine("SAISIR UN CLIENT ?");
                            string cli = Console.ReadLine();
                            foreach (Client c in lesClients)
                            {
                                if (c.NomClient == cli)
                                {
                                    foreach (Contrat contratCourant in c.ListeDesContratDuClient())
                                    {
                                        Console.WriteLine("Numero Contrat : {0}, Date Contrat : {1}",contratCourant.NumContrat,contratCourant.DateContrat);

                                    }
                                    bool testContrat = false;
                                    do
                                    {
                                        Console.WriteLine("SAISIR NUMERO CONTRAT");
                                        int numContrat = Convert.ToInt32(Console.ReadLine());

                                        foreach (Contrat contratCourant in c.ListeDesContratDuClient())
                                        {
                                            if (numContrat == contratCourant.NumContrat)
                                            {
                                                c.SuprimmerContrat(contratCourant);
                                                testContrat = true;
                                                break;
                                            }

                                        }
                                        
                                        saisieC = true;

                                    } while (testContrat == false);

                                }
                            }
                        } while (saisieC == false);
                        break;

                    case "7":
                        foreach (Client c in lesClients)
                        {
                            Console.WriteLine(c.NomClient);

                        }


                        Console.WriteLine("SAISIR UN CLIENT ?");
                        string client = Console.ReadLine();
                        foreach (Client c in lesClients)
                        {
                            if (c.NomClient == client)
                            {
                                foreach (Contrat contratCourant in c.ListeDesContratDuClient())
                                {
                                    Console.WriteLine("Numero Contrat : {0}", contratCourant.NumContrat);

                                }


                                bool testContrat = false;
                                do
                                {
                                    Console.WriteLine("SAISIR NUMERO CONTRAT");
                                    int numContrat = Convert.ToInt32(Console.ReadLine());

                                    Contrat contrat = null;

                                    foreach (Contrat contratCourant in c.ListeDesContratDuClient())
                                    {
                                        if (numContrat == contratCourant.NumContrat)
                                        {
                                            contrat = c.ObtenirContrat(numContrat);
                                            testContrat = true;
                                            break;
                                        }

                                    }

                                    Console.WriteLine("Montant contrat {0} ", contrat.MontantContrat);
                                    Console.WriteLine("Date contrat {0}", contrat.DateContrat);

                                    saisieC = true;

                                } while (testContrat == false);

                            }
                        }
                        break;

                    default:
                        break;

                }
            } while (fermerLeProgramme == false);

            


        }
    }
}
