﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CasKes
{
    class Contrat
    {
        private int _numContrat;
        private DateTime _dateContrat;
        private Client _leClient;
        private float _montantContrat;
        private List<Intervention> _collectionIntervention;

        public Contrat(int numContrat, DateTime dateContrat,float montantContrat, Client unClient)
        {
            _numContrat = numContrat;
            _dateContrat = dateContrat;
            _montantContrat = montantContrat;
            _leClient = unClient;
            _collectionIntervention = new List<Intervention>();
        }

        public int NumContrat { get => _numContrat;}
        public DateTime DateContrat { get => _dateContrat; }
        public float MontantContrat { get => _montantContrat;  }
        internal Client LeClient { get => _leClient;}
        internal List<Intervention> CollectionIntervention { get => _collectionIntervention; }

        public void AjouterIntervention(Intervention uneIntervention)
        {
            _collectionIntervention.Add(uneIntervention);

        }

        public void SuprimmerIntervention(Intervention uneIntervention)
        {

            _collectionIntervention.Remove(uneIntervention);

        }

        public Intervention ObtenirIntervention(int numIntervention)
        {
            Intervention uneIntervention = null;
            foreach (Intervention interventionCourant in _collectionIntervention)
            {
                if (interventionCourant.NumIntervention == numIntervention)
                {
                    uneIntervention = interventionCourant;
                   
                }
            }

            return uneIntervention;     
        }

        public float CoutTotalDesIntervention()
        {
            float cout = 0;
           foreach(Intervention i in _collectionIntervention)
           {
                 cout =+ i.FraisKm() + i.FraisMo();
           }

           return cout;
        }

        public float Ecart()
        {
            float ecart = _montantContrat - CoutTotalDesIntervention();
            return ecart;
        }



        private int RetourneLeProchaineNumeroDeContrat()
        {
            int max = 0;
            if (_collectionIntervention.Count > 0)
            {
                max = _collectionIntervention[0].NumIntervention;
                foreach (Intervention i in _collectionIntervention)
                {
                    if (i.NumIntervention > max)
                    {
                        max = i.NumIntervention;
                    }
                }
            }
            return max + 1;
        }
    }
}
