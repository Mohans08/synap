﻿using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;
using System.Collections.Generic;


namespace CasKes
{
    class Persistance
    {
        private static string repertoireApplication = Environment.CurrentDirectory + @"\";

        #region PARTIE CLIENT
        //public static void SauvegarderClients(List<Client> lesClients)
        //{
        //    FileStream file = null;
        //    file = File.Open(repertoireApplication + "lesClients", FileMode.OpenOrCreate);
        //    BinaryFormatter formatter = new BinaryFormatter();
        //    formatter.Serialize(file, lesClients);
        //    file.Close();
        //}

        //public static List<Client> ChargerClients()
        //{
        //    List<Client> lesClients = null;
        //    FileStream fs = null;
        //    fs = new FileStream(repertoireApplication + "lesClients", FileMode.OpenOrCreate);
        //    BinaryFormatter formatter = new BinaryFormatter();
        //    try
        //    {
        //        lesClients = formatter.Deserialize(fs) as List<Client>;
        //    }
        //    catch (SerializationException se)
        //    {

        //    }

        //    fs.Close();
        //    return lesClients;
        //}
        
        #endregion
        
        
        public static void SauvegarderEmployes(List<Employe> lesEmployes)
        {
            FileStream file = null;
            file = File.Open(repertoireApplication + "lesEmployes", FileMode.OpenOrCreate);
            BinaryFormatter formatter = new BinaryFormatter();
            formatter.Serialize(file, lesEmployes);
            file.Close();
        }

        public static List<Employe> ChargerEmployes()
        {
            List<Employe> lesEmployes = null;
            FileStream fs = null;
            fs = new FileStream(repertoireApplication + "lesEmployes", FileMode.OpenOrCreate);
            BinaryFormatter formatter = new BinaryFormatter();
            try
            {
                lesEmployes = formatter.Deserialize(fs) as List<Employe>;
            }
            catch (SerializationException se)
            {

            }

            fs.Close();
            return lesEmployes;
        }


        public static void SauvegarderClients(List<Client> lesClients)
        {
            FileStream file = null;
            file = File.Open(repertoireApplication + "lesClients", FileMode.OpenOrCreate);
            BinaryFormatter formatter = new BinaryFormatter();
            formatter.Serialize(file, lesClients);
            file.Close();
        }
        public static List<Client> ChargerClients()
        {
            List<Client> lesClients = null;
            FileStream fs = null;
            fs = new FileStream(repertoireApplication + "lesClients", FileMode.OpenOrCreate);
            BinaryFormatter formatter = new BinaryFormatter();
            try
            {
                lesClients = formatter.Deserialize(fs) as List<Client>;
            }
            catch (SerializationException se)
            {

            }

            fs.Close();
            return lesClients;
        }
    }
}
