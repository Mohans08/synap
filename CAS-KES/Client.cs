﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;

namespace CasKes
{
    class Client
    {
        private int _numClient;
        private string _nomClient;
        private string _adresseClient;
        private string _codePostal;
        private string _ville;
        private float _nbKmEntreAgenceEtClient;
        private List<Contrat> _collectionContrat;

        public Client(int numClient, string nomClient, string adresseClient, string codePostal, string ville, float nbKmEntreAgenceEtClient)
        {
            _numClient = numClient;
            _nomClient = nomClient;
            _adresseClient = adresseClient;
            _codePostal = codePostal;
            _ville = ville;
            _nbKmEntreAgenceEtClient = nbKmEntreAgenceEtClient;
            _collectionContrat = new List<Contrat>();

        }

        public int NumClient { get => _numClient; }
        public string NomClient { get => _nomClient; }
        public string AdresseClient { get => _adresseClient; set => _adresseClient = value; }
        public string CodePostal { get => _codePostal; set => _codePostal = value; }
        public string Ville { get => _ville; set => _ville = value; }


        public float GetDistanceEntreClientEtAgence()
        {
            return _nbKmEntreAgenceEtClient;
        }


        public void AjouterContrat(float montantContrat, DateTime dateContrat)
        {
            Contrat unContrat = new Contrat(this.RetourneLeProchaineNumeroDeContrat(), dateContrat, montantContrat, this);
            //_collectionContrat.Add(unContrat);

                if(_collectionContrat.Exists(cont => cont != unContrat))
                {
                    _collectionContrat.Add(unContrat);

                }
        }

        public void SuprimmerContrat(Contrat unContrat)
        {
            _collectionContrat.Remove(unContrat);
        }

        public Contrat ObtenirContrat(int numContrat)
        {
            Contrat unContrat = null;
            foreach (Contrat interventionCourant in _collectionContrat)
            {
                if (interventionCourant.NumContrat == numContrat)
                {
                    unContrat = interventionCourant;

                }
            }

            return unContrat;
        }

        private int RetourneLeProchaineNumeroDeContrat()
        {
            int max = 0;
            if (_collectionContrat.Count > 0)
            {
                max = _collectionContrat[0].NumContrat;
                foreach (Contrat c in _collectionContrat)
                {
                    if (c.NumContrat > max)
                    {
                        max = c.NumContrat;
                    }
                }
            }
            return max + 1;
        }

        public ReadOnlyCollection<Contrat> ListeDesContratDuClient()
        {  
            return new ReadOnlyCollection<Contrat>(_collectionContrat);
        }

        public Contrat this[int numContrat]
        {
            get
            {
                Contrat contrat = _collectionContrat.Find(c => c.NumContrat == numContrat);
                return contrat;
            }
        }




    }
}