﻿using System;

namespace CasKes
{
    [Serializable]
    class Grade
    {
        private string _nomGrade;
        private float _coutHoraireGrade;

        public Grade(string nomGrade, float coutHoraireGrade)
        {
            _nomGrade = nomGrade;
            _coutHoraireGrade = coutHoraireGrade;
        }

        public float CoutHoraireGrade { get => _coutHoraireGrade;}
        public string NomGrade { get => _nomGrade;}
    }
}