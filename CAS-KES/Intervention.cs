﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CasKes
{
    class Intervention
    {
        private int _numIntervention;
        private DateTime _date;
        private int _duree;
        private Employe _technicien;
        private CategorieVehicule _vehiculeUtilise;
        private Contrat _contratConcerne;


        public Intervention(int numInterventon, Employe technicien,int duree, DateTime dateIntervention, CategorieVehicule vehiculeUtilise, Contrat cotratConcerne)
        {
            _numIntervention = numInterventon;
            _technicien = technicien;
            _duree = duree;
            _date = dateIntervention;
            _vehiculeUtilise = vehiculeUtilise;
            _contratConcerne = cotratConcerne;
        }

        public int NumIntervention { get => _numIntervention; }
        public DateTime Date { get => _date;  }
        public int Duree { get => _duree; }
        internal Employe Technicien { get => _technicien;}
        internal CategorieVehicule VehiculeUtilise { get => _vehiculeUtilise; }
        internal Contrat ContratConcerne { get => _contratConcerne; set => _contratConcerne = value; }

        public float FraisKm()
        {
            float fraisKm = 0;
            float distance = _contratConcerne.LeClient.GetDistanceEntreClientEtAgence();
            float tarif = _vehiculeUtilise.TarifKilometrique;

            fraisKm = distance * tarif;

            return fraisKm;
        }

        public float FraisMo()
        {
            float fraisMo = 0;
            float cout = _technicien.CoutHoraire();
            fraisMo = _duree * cout;

            return fraisMo;
        }



    }
}
