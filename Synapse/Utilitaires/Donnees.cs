﻿using System;
using System.Collections.Generic;
using System.Text;
using Synapse.Metier;
namespace Synapse.Utilitaires
{
    [Serializable()]
    class Donnees
    {

        private static List<Intervenant> _collectionIntervenant;
        private static List<Projet> _collectionProjet;


        public static List<Intervenant> CollectionIntervenant
        {
            get
            {
                if (_collectionIntervenant == null)
                {
                    _collectionIntervenant = (List<Intervenant>)Persistances.ChargerDonnees("Intervenant");
                    if (_collectionIntervenant == null)
                        _collectionIntervenant = new List<Intervenant>();
                }
                return Donnees._collectionIntervenant;
            }
            set { Donnees._collectionIntervenant = value; }
        }


        public static List<Projet> CollectionProjet
        {
            get
            {
                if (_collectionProjet == null)
                {
                    _collectionProjet = (List<Projet>)Persistances.ChargerDonnees("Projet");
                    if (_collectionProjet == null)
                        _collectionProjet = new List<Projet>();
                }
                return Donnees._collectionProjet;
            }
        }


    }
}
