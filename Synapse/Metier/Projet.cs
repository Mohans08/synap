﻿using System;
using System.Collections.Generic;
using System.Text;
namespace Synapse.Metier
{
    class Projet
    {
        private string _nom;
        private DateTime _dateDebut;
        private DateTime _dateFin;
        private decimal _prixFactureMo;
        List<Mission> _collectionMission;

       

       
        public decimal PrixFactureMo { get => _prixFactureMo; set => _prixFactureMo = value; }
        public string Nom { get => _nom; set => _nom = value; }
        public DateTime DateDebut { get => _dateDebut; set => _dateDebut = value; }
        public DateTime DateFin { get => _dateFin; set => _dateFin = value; }

        private decimal CumulCoutMo()
        {
            decimal coutcumul = 0;   

            foreach (Mission mission in _collectionMission)
            {

                coutcumul +=  mission.NbHeuresEffectues() * mission.Intervenant.TauxHoraire;

            }

            return coutcumul;
        }
        public decimal MargeBruteCourante()
        {
            decimal margeBrute = 0;

            decimal nbHeure = 0;
            decimal prix = 0;
            foreach (Mission mission in _collectionMission)
            {
                nbHeure += mission.NbHeuresPrevues;
                prix += mission.Intervenant.TauxHoraire;
            }

            decimal margeEntrprise = nbHeure * prix;

            margeBrute = _prixFactureMo - margeEntrprise;

            return margeBrute;
        }
    }
}
