﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Synapse.Metier
{
    class Mission
    {
        private Dictionary<DateTime, int> _releveHoraire;
        private decimal _nbHeuresPrevues;
        private string _description;
        private string _nom;
        private Intervenant _intervenant;


        public Dictionary<DateTime, int> ReleveHoraire { get => _releveHoraire; set => _releveHoraire = value; }

        public decimal NbHeuresPrevues { get => _nbHeuresPrevues; set => _nbHeuresPrevues = value; }
        public string Description { get => _description; set => _description = value; }
        public string Nom { get => _nom; set => _nom = value; }
        public Intervenant Intervenant { get => _intervenant; set => _intervenant = value; }



        public void AjouteReleve(DateTime date, int nbHeures)
        {
            _releveHoraire.Add(date, nbHeures);
        }

        public int NbHeuresEffectues()
        {
            int nbHeuresEffectues = 0 ;

            foreach (KeyValuePair<DateTime, int> dic in _releveHoraire)
            {
                nbHeuresEffectues += dic.Value;
            }


            return nbHeuresEffectues;
        }
    }
}
