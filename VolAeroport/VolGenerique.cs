﻿using System;
using System.Collections.Generic;

namespace VolAeroport
{

    public class VolGenerique
    {
        private Aeroport _aeroportArrivee;
        private Aeroport _aeroportDepart;
        private float _coutVolGenerique;
        private List<CompagnieAerienne> _compagnie;

        public Aeroport Depart
        {
            get { return _aeroportArrivee; }
        }

       
        public Aeroport Arrivee
        {
            get { return _aeroportDepart; }
        }

        public float CoutVolGenerique { get => _coutVolGenerique; set => _coutVolGenerique = value; }

        public VolGenerique(Aeroport aeroportDepart, Aeroport aeroportArrivee)
        {
            _aeroportArrivee = aeroportArrivee;
            _aeroportDepart = aeroportDepart;
        }

       
        public bool Equals(VolGenerique unVolGenerique)
        {
            return  _aeroportArrivee.GetCode() == unVolGenerique.Arrivee.GetCode() 
                && _aeroportDepart.GetCode() == unVolGenerique.Depart.GetCode();            
        }

        public float CoutTotal()
        {
            float coutTotal = 0;
            coutTotal = this.Depart.GetCoutDeLaRotation() + this.Arrivee.GetCoutDeLaRotation() + this._coutVolGenerique;
            return coutTotal;
        }

        public void AjouterCompagineVolGenrerique(CompagnieAerienne compagnie)
        {
            
            _compagnie.Add(compagnie);
            compagnie.AjouterVolGenerique(this);
            
        }





    }

}