﻿using System;
using System.Collections.Generic;

namespace VolAeroport
{
   
    public class CompagnieAerienne
    {
        private string _nomCompagnie;
        private List<VolGenerique> _lignesDesservies;

        public List<VolGenerique> LignesDesservies { get => _lignesDesservies; }

        public bool Existe(VolGenerique unVolGenerique)
        {
            bool existe = false;
            foreach (VolGenerique v in _lignesDesservies)
            {
                if (v.Equals(unVolGenerique))
                {
                    existe = true;
                    break;
                }
            }
            return existe;
        }
                  
        public void AjouterVolGenerique(VolGenerique volGenerique)
        {
            if (this.Existe(volGenerique) == false)
            {
                _lignesDesservies.Add(volGenerique);
                volGenerique.AjouterCompagineVolGenrerique(this);
            }
        }

      
        public void SupprimerVolGenerique(VolGenerique UnVolGenerique)
        {
            foreach (VolGenerique v in _lignesDesservies)
            {
                if (v.Equals(UnVolGenerique))
                {
                    _lignesDesservies.Remove(v);
                    break;
                }
            }
        }

       
        public List<VolGenerique> AuDepartDe(Aeroport aeroportDeDepart)
        {
            List<VolGenerique> listeVols = new List<VolGenerique>();
            foreach (VolGenerique v in _lignesDesservies)
            {
                if(v.Depart.GetCode() == aeroportDeDepart.GetCode())
                {
                    listeVols.Add(v);
                }
            }
            return listeVols;
        }

        public float CoutTotalDesVolGenerique()
        {
            float coutTotalDesVolsGenerique = 0;
            foreach (VolGenerique v in _lignesDesservies)
            {
                coutTotalDesVolsGenerique += v.CoutTotal();
            }
            return coutTotalDesVolsGenerique;
        }

    }

}



