﻿using System;

namespace VolAeroport
{
    /// <summary>
    /// Représente un Aéroport
    /// </summary>
    public class Aeroport
    {

        private string _code;
        private string _nom;
        private string _ville;
        private string _pays;
        private float _coutDeLaRotation;

        public string GetPays()
        {
            return _pays;
        }

        public string GetNom()
        {
            return _nom;
        }

       
        public string GetVille()
        {
            return _ville;
        }

        public string GetCode()
        {
            return _code;
        }
               
        public Aeroport(string code, string nom, string ville, string pays)
        {
            _nom = nom;
            _code = code;
            _ville = ville;
            _pays = pays;
            _coutDeLaRotation = 0;
        }

        public bool Equals(Aeroport unAeroport)
        {
            bool memeAeroport = false;
            if (this._code == unAeroport._code)
            {
                memeAeroport = true;
            }
            return memeAeroport;
        }

        public void SetCoutDeLaRotation(float unCoutDeRotation)
        {
            _coutDeLaRotation = unCoutDeRotation;
        }
   
        public float GetCoutDeLaRotation()
        {
            return _coutDeLaRotation;
        }

        

    }

}