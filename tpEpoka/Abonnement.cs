﻿using System;
using System.Collections.Generic;
using System.Text;

namespace tpEpoka
{
    class Abonnement
    {
        private int _numero;
        private string _nom;
        private string _prenom;
        private string _raisonSociale;
        private string _adresse;
        private string _codePostal;
        private string _ville;
        private DateTime _dateDebut;
        private DateTime _dateFin;



        public int Numero
        {
            get { return _numero; }
        }
        public DateTime DateFin
        {
            get { return _dateFin; }
            set { _dateFin = value; }
        }
        public DateTime DateDebut
        {
            get { return _dateDebut; }
            set { _dateDebut = value; }
        }
        public Abonnement(int numAbonnement, string nomTitulaireAbonnement, string prenomTitulaireAbonnement,
            string raisonSociale, string adr, string cp, string ville, DateTime dateDebutAbo, DateTime dateFinAbo)
        {
            _numero = numAbonnement;
            _nom = nomTitulaireAbonnement;
            _prenom = prenomTitulaireAbonnement;
            _raisonSociale = raisonSociale;
            _adresse = adr;
            _codePostal = cp;
            _ville = ville;
            _dateDebut = dateDebutAbo;
            _dateFin = dateFinAbo;



        }

        public virtual decimal PrixAbonnement(Revue uneRevue)
        {
            decimal prix = 0;
            if(Localisation() == 'D')
            {
                prix = uneRevue.PrixAbonnementPublic * 1.5m;
            }
            else
            {
                prix = uneRevue.PrixAbonnementPublic;

            }

            return prix;
        }

        private char Localisation()
        {
            char localisation = 'M';
            bool success = Int32.TryParse(_codePostal, out int code); 

            if (success)
            {
                if(code >= 97100 && code <= 97999)
                {
                    localisation = 'D';

                }                
            }



            return localisation;
        }
    }
}
