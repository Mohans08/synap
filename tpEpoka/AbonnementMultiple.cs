﻿using System;
using System.Collections.Generic;
using System.Text;

namespace tpEpoka
{
    class AbonnementMultiple : Abonnement
    {

        private short _nbrExemplaires;
        private decimal _tauxRemise;


        public AbonnementMultiple(int numero, string nomTitulaireAbonnement, string prenomTitulaireAbonnement, string raisonSociale, string adr, string cp, string ville, DateTime dateDebutAbo, DateTime dateFinAbo, short nbrExemp, decimal tauxRemise)
            :base( numero,  nomTitulaireAbonnement,  prenomTitulaireAbonnement,  raisonSociale,  adr,  cp,  ville,  dateDebutAbo,  dateFinAbo)
        {
            _nbrExemplaires = nbrExemp;
            _tauxRemise = tauxRemise;
        }

        public short NbrExemplaires
        {
            get
            {
                return _nbrExemplaires;
            }

            set
            {
                _nbrExemplaires = value;
            }
        }

        public override decimal PrixAbonnement(Revue uneRevue)
        {
            return base.PrixAbonnement(uneRevue) + _nbrExemplaires * (1 - (_tauxRemise/100));
        }




    }
}
