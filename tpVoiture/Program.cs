﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;


namespace tpVoiture
{
    class Program
    {

        internal static Timer timer;
        static void Main(string[] args)
        {

            string uneMarque = "Peugeot";
            string unModele = "206";
            decimal consommation = 4;
            int quantiteCarburant = 90;



            Voiture Unevoiture = new Voiture(uneMarque, unModele, consommation, quantiteCarburant); 
            Unevoiture.VitesseMax = 200;
            Unevoiture.DemarrerVoiture();
            Console.WriteLine("{0}, {1}, {2}, {3}, {4}, {5}, {6}", Unevoiture.Marque, Unevoiture.Modele,Unevoiture.Vitesse,Unevoiture.VitesseMax, Unevoiture.ConsommationEnLitreParTrancheDeCentKilometre,Unevoiture.QuantiteCarburant,Unevoiture.Demmarer);
            Unevoiture.Accelerer(50);
            Console.WriteLine("{0}, {1}, {2}, {3}, {4}, {5}, {6}", Unevoiture.Marque, Unevoiture.Modele, Unevoiture.Vitesse, Unevoiture.VitesseMax, Unevoiture.ConsommationEnLitreParTrancheDeCentKilometre, Unevoiture.QuantiteCarburant, Unevoiture.Demmarer);
            object f = Unevoiture as Voiture;

            Simulateur(f);

            Console.Read();
            
        }


        private static void Simulateur(object unObjet)
        {

            Voiture v = unObjet as Voiture;
            Console.Write("Kilomètre Compteur : {0}", v.KilometreParcourue);
            Console.Write("Quantité Carburant : {0}", v.QuantiteCarburant);

            timer = new Timer(Simulateur, v, 0, 2000);

        }


  
    }
}
