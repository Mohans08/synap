﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace tpVoiture
{
    class Voiture
    {
        private string _marque;
        private string _modele;
        private float _vitesse;
        private float _vitesseMaxi;
        private bool _demmarer;
        private int _quantiteCarburant;
        private int _kilometreParcourue;
        private decimal _consommationEnLitreParTrancheDeCentKilometre;
        private Timer _timer;

        public Voiture(string uneMarque, string unModele, decimal consommationEnLitreParTrancheDeCentKilometre,float vitesseMaxi)
        {
            this._marque = uneMarque;
            this._modele = unModele;
            this._vitesseMaxi = vitesseMaxi;
            this._consommationEnLitreParTrancheDeCentKilometre = consommationEnLitreParTrancheDeCentKilometre;
            this._demmarer = false;
            this._vitesse = 0;

        }

        public Voiture(string uneMarque, string unModele, decimal consommationEnLitreParTrancheDeCentKilometre, int quantiteCarburant)
        {
            this._marque = uneMarque;
            this._modele = unModele;
            this._consommationEnLitreParTrancheDeCentKilometre = consommationEnLitreParTrancheDeCentKilometre;
            this._quantiteCarburant = quantiteCarburant;
            this._demmarer = false;
            this._vitesse = 0;
            this._kilometreParcourue = 0;
        }

        public string Marque { get => _marque; set => _marque = value; }
        public string Modele
        {
            get { return _modele; }
            set { _modele = value; }
        }

        public float Vitesse
        {
            get { return _vitesse; }
            set { _vitesse = value; }

        }

        public float VitesseMax
        {
            get { return _vitesseMaxi; }
            set { _vitesseMaxi = value; }
        }

        public bool Demmarer
        {
            set { _demmarer = value; }
            get { return _demmarer; }
        }

        public int QuantiteCarburant
        {
            set { _quantiteCarburant = value; }
            get { return _quantiteCarburant; }

        }

        public int KilometreParcourue
        {
            set { _kilometreParcourue = value; }
            get { return _kilometreParcourue; }
        }

        public decimal ConsommationEnLitreParTrancheDeCentKilometre
        {
            set { _consommationEnLitreParTrancheDeCentKilometre = value; }
            get { return _consommationEnLitreParTrancheDeCentKilometre; }

        }

        

        public void DemarrerVoiture()
        {
            _demmarer = true;
        }

        public void CouperLeContact()
        {
            _demmarer = false;
            _timer.Dispose();
            _timer = null;
        }

        public int NombreDeKilometreRestantAParcourir()
        {
            
            return _quantiteCarburant/decimal.ToInt32(_consommationEnLitreParTrancheDeCentKilometre);
        }


        private void OrdinateurDeBord(object unObjet)
        {


            if (_quantiteCarburant <= 0)
            {
                CouperLeContact();
            }
            else
            {
                _kilometreParcourue += Convert.ToInt32(_vitesse)/ 3600;
                _quantiteCarburant = _kilometreParcourue / decimal.ToInt32(_consommationEnLitreParTrancheDeCentKilometre) * 100; 

              
            }


        }

        private void EnRoute()
        {
            _timer = new Timer(OrdinateurDeBord, null, 0, 1000);
        }








        public void Accelerer(float vitesseEnPlus)
        {
            if (_demmarer == true)
            {
                this._vitesse += vitesseEnPlus;
                if(this._vitesse > this._vitesseMaxi)
                {
                    this._vitesse = this._vitesseMaxi;
                }
            }
        }

        public void Decelerer(float vitesseEnMoins)
        {
            if(_demmarer == true)
            {
                this._vitesse -= vitesseEnMoins;

                if(this._vitesse <= 0)
                {
                    this._vitesse = 0;
                }
            }
        }


    }
}
