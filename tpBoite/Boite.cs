﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace tpBoite
{
    class Boite
    {
        private decimal _hauteur;
        private decimal _largeur;
        private decimal _longueur;
        private decimal _volume;

        public Boite(decimal uneLongueur, decimal uneLargeur, decimal uneHauteur)
        {
            this._hauteur = uneHauteur;
            this._largeur = uneLargeur;
            this._longueur = uneLongueur;
            this._volume = Volume;
        }

        public decimal Hauteur { get => _hauteur; set => _hauteur = value; }
        public decimal Largeur { get => _largeur; set => _largeur = value; }
        public decimal Longueur { get => _longueur; set => _longueur = value; }
        public decimal Volume { get => _volume; set => _volume = value; }

        public double getVolume()
        {
            _volume = _largeur * _longueur * _hauteur;
            double _volumeD = decimal.ToDouble(_volume);
            return _volumeD;
        }

        public bool volumeIdentique(Boite boite, Boite boite1)
        {


            if (boite.getVolume() == boite1.getVolume())
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool estUnCube()
        {
            if(_longueur == _largeur && _hauteur == _longueur && _longueur == _largeur)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


    }
}
