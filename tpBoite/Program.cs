﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace tpBoite
{
    class Program
    {
        static void Main(string[] args)
        {
            decimal uneLongueur = 6;
            decimal uneLargeur = 5;
            decimal uneHauteur = 4;

            Boite uneBoite = new Boite(uneLongueur, uneLargeur, uneHauteur);

            decimal uneLongueur2 = 20;
            decimal uneLargeur2 = 10;
            decimal uneHauteur2 = 5;

            Boite uneBoite1 = new Boite(uneLongueur2, uneLargeur2, uneHauteur2);

            uneBoite.Hauteur = 8;
            Console.WriteLine(uneBoite.Hauteur);

            bool cube = uneBoite1.volumeIdentique(uneBoite, uneBoite1);
            Console.WriteLine(cube);

            Console.Read();

        }
    }
}
