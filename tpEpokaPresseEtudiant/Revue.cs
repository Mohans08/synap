﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;


namespace tpEpokaPresseEtudiant
{
    [Serializable]
    public class Revue
    {
        private string _code;
        private string _titre;
        private decimal _prixAbonnementPublic;
        private List<Abonnement> _lesAbonnements;

        public string Titre
        {
            get { return _titre; }
        }
        public decimal PrixAbonnementPublic
        {
            get { return _prixAbonnementPublic; }
        }
        public ReadOnlyCollection<Abonnement> LesAbonnements
        {
            get => new ReadOnlyCollection<Abonnement>(_lesAbonnements);
        }
        public Abonnement this[int numAbonnement]
        {
            get { return _lesAbonnements.Find(unAbonnement => unAbonnement.Numero == numAbonnement); }
        }

        public void AjouterAbonnement(string nomTitulaireAbonnement, string prenomTitulaireAbonnement, string raisonSociale, string adr, string cp, string ville, DateTime dateDebutAbo, DateTime dateFinAbo)
        {
            /*
              * Méthode à compléter
              * */
        }

        public void AjouterAbonnement(string nomTitulaireAbonnement, string prenomTitulaireAbonnement, string raisonSociale, string adr, string cp, string ville, DateTime dateDebutAbo, DateTime dateFinAbo, short nbrExemp, decimal tauxRemise)
        {
            /*
             * Méthode à compléter
             * */

        }
        public Revue(string unCodeRevue, string unTitre, decimal unPrixAbonnementPublic)
        {
            _code = unCodeRevue;
            _titre = unTitre;
            _prixAbonnementPublic = unPrixAbonnementPublic;
            _lesAbonnements = new List<Abonnement>();
        }
        public List<Abonnement> RelancerAbonnement()
        {
            /*
            * A compléter
            * */
            throw new NotImplementedException();//instruction à supprimer une fois la méthode complétée
        }

        public int NbExemplaireAbonnement()
        {
            /*
            * A compléter
            * */
            throw new NotImplementedException();//instruction à supprimer une fois la méthode complétée
        }

        private int ProchainNumeroAbonnement()
        {
            int max = 0;

            if (_lesAbonnements.Count > 0)
            {
                foreach (Abonnement abonnementCourant in _lesAbonnements)
                {
                    if (abonnementCourant.Numero > max)
                    {
                        max = abonnementCourant.Numero;
                    }

                }
            }

            return max + 1;
        }
    }
}
