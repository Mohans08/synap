﻿using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;
using System.Collections.Generic;


namespace tpEpokaPresseEtudiant
{
    class Persistance
    {
        private static string repertoireApplication = Environment.CurrentDirectory + @"\";
        public static void SauvegarderRevues(List<Revue> lesRevues)
        {
            FileStream file = null;
            file = File.Open(repertoireApplication + "lesRevues", FileMode.OpenOrCreate);
            BinaryFormatter formatter = new BinaryFormatter();
            formatter.Serialize(file, lesRevues);
            file.Close();
        }
        public static List<Revue> ChargerRevues()
        {
            List<Revue> lesClients = null;
            FileStream fs = null;
            fs = new FileStream(repertoireApplication + "lesRevues", FileMode.OpenOrCreate);
            BinaryFormatter formatter = new BinaryFormatter();
            try
            {
                lesClients = formatter.Deserialize(fs) as List<Revue>;
            }
            catch (SerializationException se)
            {

            }

            fs.Close();
            return lesClients;
        }
    }
}
