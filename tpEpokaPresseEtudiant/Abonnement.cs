﻿using System;
using System.Collections.Generic;

namespace tpEpokaPresseEtudiant
{
    [Serializable]
    public class Abonnement
    {
        private int _numero;
        private string _nom;
        private string _prenom;
        private string _raisonSociale;
        private string _adresse;
        private string _codePostal;
        private string _ville;
        private DateTime _dateDebut;
        private DateTime _dateFin;


        public int Numero
        {
            get { return _numero; }
        }
        public DateTime DateFin
        {
            get { return _dateFin; }
            set { _dateFin = value; }
        }
        public DateTime DateDebut
        {
            get { return _dateDebut; }
            set { _dateDebut = value; }
        }
        public Abonnement(int numAbonnement, string nomTitulaireAbonnement, string prenomTitulaireAbonnement,
            string raisonSociale, string adr, string cp, string ville, DateTime dateDebutAbo, DateTime dateFinAbo)
        {
            _numero = numAbonnement;
            _nom = nomTitulaireAbonnement;
            _prenom = prenomTitulaireAbonnement;
            _raisonSociale = raisonSociale;
            _adresse = adr;
            _codePostal = cp;
            _ville = ville;
            _dateDebut = dateDebutAbo;
            _dateFin = dateFinAbo;
            
        }
      
        public virtual decimal PrixAbonnement()
        {
            /*
             * A compléter
             * */
            throw new NotImplementedException();//instruction à supprimer une fois la méthode complétée
        }
   
        private char Localisation()
        {

            /*
             * A compléter
             * */
            throw new NotImplementedException();//instruction à supprimer une fois la méthode complétée
        }
    }
}
