﻿using System;
using System.Collections.Generic;


namespace tpEpokaPresseEtudiant
{
    
    /// <summary>
    /// Classe métier
    /// Cette classe permet la création et la gestion d'abonnement multiple
    /// </summary>    
    [Serializable]
    public class AbonnementMultiple : Abonnement
    {       
        private short _nbrExemplaires;
        private int _tauxRemise;
 
        public short NbrExemplaires
        {
            get
            {
                return _nbrExemplaires;
            }

            set
            {
                _nbrExemplaires = value;
            }
        }
      
        public override decimal PrixAbonnement()
        {
            /*
             * A compléter
             * */
            throw new NotImplementedException();//instruction à supprimer une fois la méthode complétée
        }

    }
}

