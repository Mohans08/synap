﻿using System;
using System.Collections.Generic;
using System.Text;

namespace tpKesSimpleNavigabilite
{
    class CategorieVehicule
    {
        private string _categorieVoiture;
        private float _tarifKilometrique;

        public CategorieVehicule(string categorieVehicule, float coeficientApplique)
        {
            _categorieVoiture = categorieVehicule;
            _tarifKilometrique = coeficientApplique;
        }

        public float TarifKilometrique { get => _tarifKilometrique; set => _tarifKilometrique = value; }
        public string CategorieVoiture { get => _categorieVoiture; }
    }
}

