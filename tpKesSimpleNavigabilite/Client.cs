﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Collections.ObjectModel;


namespace tpKesSimpleNavigabilite
{
    class Client
    {



        private int _numClient;
        private string _nomClient;
        private string _adresseClient;
        private string _codePostal;
        private string _ville;
        private List<Contrat> _collectionContrat;

        public Client(int numClient, string nomClient, string adresseClient, string codePostal, string ville)
        {
            _numClient = numClient;
            _nomClient = nomClient;
            _adresseClient = adresseClient;
            _codePostal = codePostal;
            _ville = ville;
            _collectionContrat = new List<Contrat>();

        }

        public int NumClient { get => _numClient; }
        public string NomClient { get => _nomClient; }
        public string AdresseClient { get => _adresseClient; set => _adresseClient = value; }
        public string CodePostal { get => _codePostal; set => _codePostal = value; }
        public string Ville { get => _ville; set => _ville = value; }




        public void AjouterContrat(float montantContrat, DateTime dateContrat)
        {
            Contrat unContrat = new Contrat(this.RetourneLeProchaineNumeroDeContrat(), dateContrat, montantContrat, this);


            bool existeContrat = false;
            foreach (Contrat contrat in _collectionContrat)
            {
                if (contrat.NumContrat == unContrat.NumContrat)
                {
                    existeContrat = true;
                    break;
                }
            }
            if (existeContrat == false)
            {
                _collectionContrat.Add(unContrat);
            }
        }
         
        public void SuprimmerContrat(Contrat unContrat)
        {
            foreach (Contrat ContratCourant in _collectionContrat)
            {
                if (unContrat == ContratCourant)
                {
                    _collectionContrat.Remove(unContrat);
                    break;

                }
            }
        }

        public Contrat ObtenirContrat(int numContrat)
        {
            Contrat unContrat = null;
            foreach (Contrat interventionCourant in _collectionContrat)
            {
                if (interventionCourant.NumContrat == numContrat)
                {
                    unContrat = interventionCourant;

                }
            }


            return unContrat;
        }

        public float MontantDesInterventionsPourUnMois(int mois)
        {
            float montantDesInterventions = 0;
            DateTime date = DateTime.Today;
            foreach (Contrat contratCourant in _collectionContrat)
            {
                foreach (Intervention interventionCourant in contratCourant.CollectionIntervention)
                {
                    if(interventionCourant.Date.Month == mois && date.Year == interventionCourant.Date.Year)
                    {
                        montantDesInterventions += interventionCourant.FraisKm() + interventionCourant.FraisMo();
                    }

                }
            }

            return montantDesInterventions;
        }

        public float MontantDesInterventionsPourUnTrimestre(int trimestre)
        {

            float montantTrimestre = 0;
            trimestre = trimestre * 3;
            int debutMois = trimestre - 3;

            if(debutMois == 0)
            {
                debutMois = 1;
            }
            else
            {
                debutMois = debutMois + 1;
            }

            for (int i = debutMois; i <= trimestre; i++)
            {
                montantTrimestre += MontantDesInterventionsPourUnMois(i);
            }
            return montantTrimestre;
        }
        private int RetourneLeProchaineNumeroDeContrat()
        {
            int max = 0;
            if (_collectionContrat.Count > 0)
            {
                max = _collectionContrat[0].NumContrat;
                foreach (Contrat c in _collectionContrat)
                {
                    if (c.NumContrat > max)
                    {
                        max = c.NumContrat;
                    }
                }
            }
            return max + 1;
        }



        public ReadOnlyCollection<Contrat> ListeDesContratDuClient()
        {
            return new ReadOnlyCollection<Contrat>(_collectionContrat);
        }

        public Contrat this[int numContrat]
        {
            get
            {
                Contrat contrat = _collectionContrat.Find(c => c.NumContrat == numContrat);
                return contrat;
            }
        }




    }
}
