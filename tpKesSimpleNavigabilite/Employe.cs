﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace tpKesSimpleNavigabilite
{
    class Employe
    {
        /*
 * A COMPLETER
 * */

        private int _numEmploye;
        private string _nomEmploye;
        private Grade _grade;
        private DateTime _dateEmbauche;

        public Employe(int numEmploye, string nomEmploye, Grade grade, DateTime dateEmbauche)
        {
            _numEmploye = numEmploye;
            _nomEmploye = nomEmploye;
            _grade = grade;
            _dateEmbauche = dateEmbauche;

        }

        public int NumEmploye { get => _numEmploye; }
        public string NomEmploye { get => _nomEmploye; }
        internal Grade Grade { get => _grade; set => _grade = value; }

        public float CoutHoraire()
        {
            /*
             * La fonction cout horaire calcul le cout horaire d'un employé en fonction de son ancienneté 
             * dans l'entreprise et de son grade
             * 
             * le cout horaire est égale au cout horaire du grade majoré en fonction de l'ancienneté. 
             * 
             * Vous utiliserez les méthodes de la classe (en réalité une structure) DateTime
             * https://docs.microsoft.com/fr-fr/dotnet/api/system.datetime?view=netframework-4.8
             * */

            float coutTotal = 0;

            int dateEmbauche = _dateEmbauche.Year;
            DateTime dateAujourdhui = DateTime.Today;

            int date = dateAujourdhui.Year;

            float anciennete = date - dateEmbauche;

            if (anciennete >= 5 && anciennete <= 10)
            {
                coutTotal = _grade.CoutHoraireGrade * (1 + (5F / 100F));

            }
            else if (anciennete >= 11 && anciennete <= 15)
            {
                coutTotal = _grade.CoutHoraireGrade * (1 + (8F / 100F));
            }
            else if (anciennete > 15)
            {
                coutTotal = _grade.CoutHoraireGrade * (1 + (12F / 100F));
            }
            else
            {
                coutTotal = _grade.CoutHoraireGrade;
            }
            return coutTotal;
        }

    }
}
