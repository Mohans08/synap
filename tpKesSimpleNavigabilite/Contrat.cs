﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace tpKesSimpleNavigabilite
{
    class Contrat
    {
        private int _numContrat;
        private DateTime _dateContrat;
        private Client _leClient;
        private float _montantContrat;
        private List<Intervention> _collectionIntervention = new List<Intervention>();
  

        public Contrat(int numContrat, DateTime dateContrat, float montantContrat, Client unClient)
        {
            _numContrat = numContrat;
            _dateContrat = dateContrat;
            _montantContrat = montantContrat;
            _leClient = unClient;
        }

        public int NumContrat { get => _numContrat; }
        public DateTime DateContrat { get => _dateContrat; }
        public float MontantContrat { get => _montantContrat; }
        internal Client LeClient { get => _leClient; }
        internal List<Intervention> CollectionIntervention { get => _collectionIntervention; }

        public void AjouterIntervention(Intervention uneIntervention)
        {
            if (_collectionIntervention.Exists(intervention => intervention.NumIntervention == uneIntervention.NumIntervention) == false)
            {
                _collectionIntervention.Add(uneIntervention);

            }
        }

        public void SuprimmerIntervention(Intervention uneIntervention)
        {

            _collectionIntervention.Remove(uneIntervention);

        }

        public Intervention ObtenirIntervention(int numIntervention) => _collectionIntervention.First(interventionCourant => interventionCourant.NumIntervention == numIntervention);

        public float CoutTotalDesIntervention()
        {
            float cout = 0;
            foreach (Intervention i in _collectionIntervention)
            {
                cout = +i.FraisKm() + i.FraisMo();
            }

            return cout;
        }

        public float Ecart()
        {
            float ecart = _montantContrat - CoutTotalDesIntervention();
            return ecart;
        }

        public Intervention InterventionLaPlusCouteuse()
        {
            Intervention uneIntervention = _collectionIntervention[0];

            foreach (Intervention interventionCourant in _collectionIntervention)
            {       
                if( interventionCourant.FraisKm() + interventionCourant.FraisMo() > uneIntervention.FraisMo() +uneIntervention .FraisKm())
                {
                    uneIntervention = interventionCourant;

                }

            }

            return uneIntervention;
        }



        private int RetourneLeProchaineNumeroDeContrat()
        {
            int max = 0;
            if (_collectionIntervention.Count > 0)
            {
                max = _collectionIntervention[0].NumIntervention;
                foreach (Intervention i in _collectionIntervention)
                {
                    if (i.NumIntervention > max)
                    {
                        max = i.NumIntervention;
                    }
                }
            }
            return max + 1;
        }
    }
}
