﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Globalization;

namespace tpKesSimpleNavigabilite
{
    class Program
    {
        static void Main(string[] args)
        {
            #region CHARGEMENT DES DONNEES
            List<Grade> lesGrades = new List<Grade>();
            lesGrades.Add(new Grade("Technicien Niveau 1", 10));
            lesGrades.Add(new Grade("Technicien Niveau 2", 11));
            lesGrades.Add(new Grade("Technicien Superieur Niveau 1", 12.5f));
            lesGrades.Add(new Grade("Technicien Superieur Niveau 2", 14f));
            lesGrades.Add(new Grade("Expert", 18));

            List<CategorieVehicule> lesCategoriesVehicule = new List<CategorieVehicule>();
            lesCategoriesVehicule.Add(new CategorieVehicule("A", 2.25f));
            lesCategoriesVehicule.Add(new CategorieVehicule("B", 2f));
            lesCategoriesVehicule.Add(new CategorieVehicule("C", 1.37f));
            lesCategoriesVehicule.Add(new CategorieVehicule("D", 1f));

            List<Client> lesClients = Persistance.ChargerClients();
            if (lesClients == null)
            {
                lesClients = new List<Client>();//Creation d'une collection de clients + jeu d'essai
                lesClients.Add(new Client(1, "SuperMag", "1 rue de la Grande Armee", "75015", "Paris"));
                lesClients.Add(new Client(2, "AuMag", "12 rue du Chêne", "95500", "Cergy"));
                lesClients.Add(new Client(3, "SuperMarch", "21 rue du Sapin", "78700", "Conflans"));
                lesClients.Add(new Client(4, "MagBio", "33 rue du Bouleau", "95680", "Enghien les bains"));
            }

            List<Employe> lesEmployes = Persistance.ChargerEmployes();
            if (lesEmployes == null)
            {
                lesEmployes = new List<Employe>();
                lesEmployes.Add(new Employe(1, "Dupont", lesGrades[0], new DateTime(2005, 10, 21)));
                lesEmployes.Add(new Employe(2, "Durant",  lesGrades[1], new DateTime(2010, 9, 12)));
                lesEmployes.Add(new Employe(3, "Duverne", lesGrades[2], new DateTime(2015, 4, 5)));
                lesEmployes.Add(new Employe(4, "Clovis", lesGrades[3],new DateTime(2012, 8, 7)));
            }
            #endregion


            #region SECTION VARIABLE GLOBAL A LA PORTEE

            //Contrat unContrat = null;
            //Client unClient = null;
            //Intervention uneIntervention = null;
            bool existe, saisieValide;
            existe = saisieValide = false;
            string chaineSaisie = null;

            #endregion

            bool fermerLeProgramme = false;
            do
            {
                #region MENU
                Console.WriteLine("1-LISTE CLIENTS");
                Console.WriteLine("2-LISTE EMPLOYES");
                Console.WriteLine("3-AJOUTER UN EMPLOYE");
                Console.WriteLine("4-AJOUTER UN CLIENT");
                Console.WriteLine("5-AJOUTER UN CONTRAT");
                Console.WriteLine("6-AJOUTER UNE INTERVENTION");
                Console.WriteLine("7-SUPPRIMER UN CLIENT");
                Console.WriteLine("8-SUPPRIMER LE CONTRAT D'UN CLIENT");
                Console.WriteLine("9-SUPPRIMER L'INTERVENTION D'UN CONTRAT");
                Console.WriteLine("10-INFO CLIENT");
                Console.WriteLine("0-SAVE AND EXIT");
                #endregion

                saisieValide = false;
                chaineSaisie = Console.ReadLine();
                switch (chaineSaisie)
                {
                    #region CASE 0 à 3
                    case "0":
                        Persistance.SauvegarderClients(lesClients);
                        Persistance.SauvegarderEmployes(lesEmployes);
                        fermerLeProgramme = true;
                        break;

                    case "1":

                        foreach (Client c in lesClients)
                        {
                            Console.WriteLine(c.NomClient);
                        }
                        
                        break;

                    case "2":
                        foreach (Employe e in lesEmployes)
                        {
                            Console.WriteLine(e.NomEmploye);
                        }
                        break;


                    case "3":
                        saisieValide = false;
                        int numEmploye = 0;
                        do
                        {
                            Console.WriteLine("NUM EMPLOYE ?");
                            chaineSaisie = Console.ReadLine();

                            if (int.TryParse(chaineSaisie, out numEmploye) == true)
                            {
                                saisieValide = true;
                            }
                        } while (saisieValide == false);


                        Console.WriteLine("NOM EMPLOYE ?");
                        string nomEmploye = Console.ReadLine();
                        Console.WriteLine("DATE EMBAUCHE ?");
                        string dateSaisie = Console.ReadLine();
                        DateTime dt = Convert.ToDateTime(dateSaisie);
                        Grade gradeEmploye = null;

                        foreach (Grade gradeCourant in lesGrades)
                        {
                            Console.WriteLine("Nom Grade : {0}", gradeCourant.NomGrade);
                        }

                        saisieValide = false;
                        do
                        {
                            Console.WriteLine("SAISIR LE NOM DE GRADE");
                            string grade = Console.ReadLine();
                            foreach (Grade g in lesGrades)
                            {
                                if (g.NomGrade == grade)
                                {
                                    gradeEmploye = g;
                                    saisieValide = true;
                                }
                            }
                        } while (saisieValide == false);
                        Employe unEmploye = new Employe(numEmploye, nomEmploye, gradeEmploye, dt);
                        lesEmployes.Add(unEmploye);
                        break;
                    #endregion

                    #region CAS 4
                    case "4":
                        int numClient = 0;
                        saisieValide = false;
                        do
                        {
                            Console.WriteLine("NUM CLIENT");
                            chaineSaisie = Console.ReadLine();

                            if (int.TryParse(chaineSaisie, out numClient) == true)
                            {
                                saisieValide = true;
                            }
                        } while (saisieValide == false);

                        Console.WriteLine("NOM CLIENT ?");
                        string nomClient = Console.ReadLine();
                        Console.WriteLine("ADRESSE CLIENT ?");
                        string adresseClient = Console.ReadLine();
                        Console.WriteLine("CODE POSTAL ?");
                        string codePostal = Console.ReadLine();
                        Console.WriteLine("VILLE ?");
                        string ville = Console.ReadLine();

                        Client unClient = new Client(numClient, nomClient, adresseClient, codePostal, ville);
                        lesClients.Add(unClient);
                        break;

                    #endregion

                    #region CAS 5 AJOUTER CONTRAT
                    case "5":
                        foreach (Client c in lesClients)
                        {
                            Console.WriteLine(c.NomClient);

                        }

                        bool saisie = false;
                        do
                        {
                            Console.WriteLine("SAISIR LE NOM DU  CLIENT ?");
                            string cli = Console.ReadLine();
                            foreach (Client c in lesClients)
                            {
                                if (c.NomClient == cli)
                                {
                                    float montantContrat = 0;
                                    bool montant = false;
                                    do
                                    {
                                        Console.WriteLine("MONTANT CONTRAT ?");
                                        chaineSaisie = Console.ReadLine();

                                        if (float.TryParse(chaineSaisie, out montantContrat) == true)
                                        {
                                            montant = true;
                                        }
                                    } while (montant == false);
                                    DateTime date = DateTime.Now;
                                    c.AjouterContrat(montantContrat, date);
                                    saisie = true;
                                    break;

                                }


                            }
                        } while (saisie == false);
                        break;
                    #endregion

                    #region CAS 6 AJOUTER UNE INTERVENTION
                    case "6":
                        
                        foreach (Employe employeCourant in lesEmployes)
                        {
                            Console.WriteLine("Nom technicien {0}", employeCourant.NomEmploye);
                        }
                        Employe technicien = null;
                        saisieValide = false;
                        do
                        {
                            Console.WriteLine("SAISIR LE NOM DU TECHNICIEN");
                            string nomTechnicien = Console.ReadLine();                            
                            foreach (Employe employeCourant in lesEmployes)
                            {
                                if (employeCourant.NomEmploye == nomTechnicien)
                                {
                                    technicien = employeCourant;
                                    saisieValide = true;
                                    break;
                                }
                            }
                        } while (saisieValide == false);

                        int duree = 0;
                        saisieValide = false;
                        do
                        {
                            Console.WriteLine("SAISIR LA DUREE ?");
                            chaineSaisie = Console.ReadLine();

                            if (int.TryParse(chaineSaisie, out duree) == true)
                            {
                                saisieValide = true;
                            }
                        } while (saisieValide == false);

                        saisieValide = false;
                        DateTime dateInter = new DateTime();
                        do
                        {
                            Console.WriteLine("SAISIR LA DATE DE L'INTERVENTION");
                            Console.WriteLine("FORMAT JJ/MM/AAAA");
                            string dateIntervention = Console.ReadLine();
                            CultureInfo frFR = new CultureInfo("fr-FR");
                            try
                            {
                                DateTime dateValue = DateTime.ParseExact(dateIntervention, "dd/MM/yyyy", frFR, DateTimeStyles.None);
                                dateInter = dateValue;
                                saisieValide = true;
                            }
                            catch (FormatException)
                            {
                                Console.WriteLine("{0} Le format est un incorrecte.", dateIntervention);
                            }

                        } while (saisieValide == false);

                        foreach (CategorieVehicule vehiculeCourant in lesCategoriesVehicule)
                        {
                            Console.WriteLine("Categorie vehicule {0}", vehiculeCourant.CategorieVoiture);
                        }
                        saisieValide = false;
                        CategorieVehicule veh = null;
                        do
                        {
                            Console.WriteLine("SAISIR UN CATEGORIE DE VEHICULE ?");
                            string catVeh = Console.ReadLine();
                            foreach (CategorieVehicule vehiculeCourant in lesCategoriesVehicule)
                            {
                                if(vehiculeCourant.CategorieVoiture == catVeh)
                                {
                                    veh = vehiculeCourant;
                                    saisieValide = true;
                                    break;
                                }
                            }

                        } while (saisieValide == false);

                        saisieValide = false;
                        float distance = 0;
                        do
                        {
                            Console.WriteLine("SAISIR LA DISTANCE D'INTERVENTION ?");
                            chaineSaisie = Console.ReadLine();

                            if (float.TryParse(chaineSaisie, out distance) == true)
                            {
                                saisieValide = true;
                            }


                        } while (saisieValide == false);

                        foreach (Client  clientCourant in lesClients)
                        {
                            Console.WriteLine("Nom client : {0}, Num Client : {1}",clientCourant.NomClient, clientCourant.NumClient);
                            
                        }

                        saisieValide = false;
                        do
                        {

                            int numero = 0;
                            bool m = false;
                            do
                            {
                                Console.WriteLine("NUM CLIENT ?");
                                chaineSaisie = Console.ReadLine();

                                if (int.TryParse(chaineSaisie, out numero) == true)
                                {
                                    m = true;
                                }
                            } while (m == false);


                            foreach (Client clientCourant in lesClients)
                            {
                                if (clientCourant.NumClient == numero)
                                {
                                    foreach (Contrat contratCourant in clientCourant.ListeDesContratDuClient())
                                    {
                                        Console.WriteLine("Numero de contrat :{0}",contratCourant.NumContrat);
                                    }

                                    m = false;
                                    int numC = 0;
                                    do
                                    {
                                        Console.WriteLine("SAISIR LE NUMERO DU CONTRAT ?");
                                        chaineSaisie = Console.ReadLine();

                                        if (int.TryParse(chaineSaisie, out numC) == true)
                                        {
                                            m = true;
                                        }
                                    } while (m == false);

                                    foreach (Contrat contratCourant in clientCourant.ListeDesContratDuClient())
                                    {
                                        if (contratCourant.NumContrat == numC)
                                        {
                                            
                                            int num = contratCourant.CollectionIntervention.Count;
                                            Intervention uneIntervention = new Intervention(num + 1, technicien, duree, dateInter, veh, distance);
                                            contratCourant.AjouterIntervention(uneIntervention);
                                            saisieValide = true;    
                                            break;
                                        }
                                    }
                                    break;
                                }
                            }

                        } while (saisieValide == false);
                        break;

                    #endregion


                    #region CAS 7 INFO CLIENT
                    case "7":
                        saisieValide = false;
                        foreach (Client clientCourant in lesClients)
                        {
                            Console.WriteLine("Nom client : {0}; Num Client {1} ", clientCourant.NomClient, clientCourant.NumClient);
                        }
                        do
                        {
                            Console.WriteLine("SAISIR LE NOM DU CLIENT ?");
                            string cli = Console.ReadLine();
                            foreach (Client c in lesClients)
                            {
                                if (c.NomClient == cli)
                                {
                                    lesClients.Remove(c);
                                    saisieValide = true;
                                    break;      
                                }
                            }
                        } while (saisieValide == false);
                        break;
                    case "8":
                        foreach (Client clientCourant in lesClients)
                        {
                            Console.WriteLine("Nom client : {0}; Num Client {1} ",clientCourant.NomClient, clientCourant.NumClient);
                        }
                        saisieValide = false;

                        do
                        {
                            Console.WriteLine("SAISIR UN NUMERO CLIENT ?");

                            int nmClient = 0;
                            bool traitement = false;
                            do
                            {
                                Console.WriteLine("NUM CLIENT");
                                chaineSaisie = Console.ReadLine();

                                if (int.TryParse(chaineSaisie, out nmClient) == true)
                                {
                                    traitement = true;
                                }
                            } while (traitement == false);


                            foreach (Client c in lesClients)
                            {
                                if (c.NumClient == nmClient)
                                {
                                    foreach (Contrat contratCourant in c.ListeDesContratDuClient())
                                    {
                                        Console.WriteLine("Numero Contrat : {0}, Date Contrat : {1}", contratCourant.NumContrat, contratCourant.DateContrat);

                                    }
                                    bool testContrat = false;
                                    do
                                    {
                                        Console.WriteLine("SAISIR NUMERO CONTRAT");
                                        int numContrat = Convert.ToInt32(Console.ReadLine());

                                        foreach (Contrat contratCourant in c.ListeDesContratDuClient())
                                        {
                                            if (numContrat == contratCourant.NumContrat)
                                            {
                                                c.SuprimmerContrat(contratCourant);
                                                testContrat = true;
                                                break;
                                            }

                                        }

                                        saisieValide = true;

                                    } while (testContrat == false);

                                }
                            }
                        } while (saisieValide == false);
                        break;
                    case "9":

                        foreach (Client clientCourant in lesClients)
                        {
                            Console.WriteLine("Nom client : {0}; Num Client {1} ", clientCourant.NomClient, clientCourant.NumClient);
                        }

                        saisieValide = false;

                        do
                        {
                            Console.WriteLine("SAISIR UN NUMERO CLIENT ?");
                            int nmClient = Convert.ToInt32(Console.ReadLine());
                            foreach (Client c in lesClients)
                            {
                                if (c.NumClient == nmClient)
                                {
                                    foreach (Contrat contratCourant in c.ListeDesContratDuClient())
                                    {
                                        Console.WriteLine("Numero Contrat : {0}, Date Contrat : {1}", contratCourant.NumContrat, contratCourant.DateContrat);

                                        foreach(Intervention interventionCourant in contratCourant.CollectionIntervention)
                                        {
                                            Console.WriteLine("--Numero d'intervention : {0}", interventionCourant.NumIntervention);
                                        }
                                        
                                    }
                                    bool testContrat = false;
                                    do
                                    {
                                        Console.WriteLine("SAISIR NUMERO CONTRAT");
                                        int numContrat = Convert.ToInt32(Console.ReadLine());

                                        Console.WriteLine("SAISIR NUMERO D'INTERVENTION");
                                        int numIn = Convert.ToInt32(Console.ReadLine());

                                        foreach (Contrat contratCourant in c.ListeDesContratDuClient())
                                        {
                                            if(contratCourant.NumContrat == numContrat)
                                            {
                                                foreach (Intervention interventionCourant in contratCourant.CollectionIntervention)
                                                {
                                                    if(interventionCourant.NumIntervention == numIn)
                                                    {
                                                        contratCourant.SuprimmerIntervention(interventionCourant);
                                                        testContrat = true;
                                                        break;
                                                    }
                                                }

                                                break;
                                            }

                                        }

                                        saisieValide = true;

                                    } while (testContrat == false);

                                }
                            }
                        } while (saisieValide == false);
                        break;
                    case "10":
                        foreach (Client clientCourant in lesClients)
                        {
                            Console.WriteLine("Nom client : {0}; Num Client {1} ", clientCourant.NomClient, clientCourant.NumClient);
                        }

                        Console.WriteLine("SAISIR UN NUMERO CLIENT ?");
                        int client = Convert.ToInt32(Console.ReadLine());

                        foreach (Client clientCourant in lesClients)
                        {
                            if(clientCourant.NumClient == client)
                            {
                                Console.WriteLine("nom Client: {0}; ville Client: {1}, code Postal: {2},adresse Client: {3}",clientCourant.NomClient, clientCourant.Ville, clientCourant.CodePostal, clientCourant.AdresseClient);

                                foreach (Contrat contratCourant  in clientCourant.ListeDesContratDuClient())
                                {
                                    Console.WriteLine("Numero Contrat : {0}; Montant Contrat : {1}", contratCourant.NumContrat, contratCourant.MontantContrat);
                                    foreach (Intervention interventionCourant in contratCourant.CollectionIntervention)
                                    {
                                        Console.WriteLine("--Numero Intervention : {0}; Date Intervention : {1}; Duree Intervention {2}", interventionCourant.NumIntervention, interventionCourant.Date, interventionCourant.Duree);
                                    }
                                }

                                Console.WriteLine("Saisir un mois");
                                int mois = Convert.ToInt32(Console.ReadLine());
                                Console.WriteLine("Montant des interventions est de {0} pour le mois de {1} ",clientCourant.MontantDesInterventionsPourUnMois(mois),mois);

                                Console.WriteLine("Saisir un trmistre");
                                int trimestre = Convert.ToInt32(Console.ReadLine());
                                Console.WriteLine("Montant des interventions est de {0} pour le trimestre de {1} ", clientCourant.MontantDesInterventionsPourUnTrimestre(trimestre), trimestre);

                                break;
                            }
                        }


                        break;
                    #endregion 
                    default:


                        break;
                }
            } while (fermerLeProgramme == false);
        }
    }
}
