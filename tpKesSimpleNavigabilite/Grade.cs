﻿using System;
using System.Collections.Generic;
using System.Text;

namespace tpKesSimpleNavigabilite
{
    class Grade
    {
        private string _nomGrade;
        private float _coutHoraireGrade;

        public Grade(string nomGrade, float coutHoraireGrade)
        {
            _nomGrade = nomGrade;
            _coutHoraireGrade = coutHoraireGrade;
        }

        public float CoutHoraireGrade { get => _coutHoraireGrade; set => _coutHoraireGrade = value; }
        public string NomGrade { get => _nomGrade; }
    }
}
