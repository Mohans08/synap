﻿using System;
using System.Collections.Generic;
using System.Text;

namespace tpKesSimpleNavigabilite
{
    class Intervention
    {
        private int _numIntervention;
        private DateTime _date;
        private int _duree;
        private Employe _technicien;
        private CategorieVehicule _vehiculeUtilise;
        private float _distanceIntervention;


        public Intervention(int numInterventon, Employe technicien, int duree, DateTime dateIntervention, CategorieVehicule vehiculeUtilise, float distanceIntervention)
        {
            _numIntervention = numInterventon;
            _technicien = technicien;
            _duree = duree;
            _date = dateIntervention;
            _vehiculeUtilise = vehiculeUtilise;
            _distanceIntervention = distanceIntervention;
        }

        public int NumIntervention { get => _numIntervention; }
        public DateTime Date { get => _date; }
        public int Duree { get => _duree; }
        internal Employe Technicien { get => _technicien; }
        internal CategorieVehicule VehiculeUtilise { get => _vehiculeUtilise; }
     

        public float FraisKm()
        {
            float fraisKm = 0;

            float tarif = _vehiculeUtilise.TarifKilometrique;

            fraisKm = _distanceIntervention * tarif;

            return fraisKm;
        }

        public float FraisMo()
        {
            float fraisMo = 0;
            float cout = _technicien.CoutHoraire();
            fraisMo = _duree * cout;

            return fraisMo;
        }



    }
}
