﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace tpBanque
{
    class Client
    {
        private string _numClient;
        private string _nomClient;
        private List<Compte> _collectionCompte;

        public Client (string numClient, string nomClient)
        {
            _numClient = numClient;
            _nomClient = nomClient;
            _collectionCompte = new List<Compte>();
        }

        public void AjouterCompte(Compte unCompte)
        {
            _collectionCompte.Add(unCompte);
            unCompte.Titulaire = this;
        }

        public void TransfererCompte(Client unClient, string NumCompte)
        {
            foreach (Compte collectionCompte in _collectionCompte)
            {
                if (collectionCompte.NumCompte == NumCompte)
                {
                    unClient.AjouterCompte(collectionCompte);
                    _collectionCompte.Remove(collectionCompte);
                }
            }

        }






        public string NumClient { get => _numClient;  }
        public string NomClient { get => _nomClient; }

    }
}
