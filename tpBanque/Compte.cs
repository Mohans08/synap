﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace tpBanque
{
    class Compte
    {
        private string _numCompte;
        private Client _titulaire;
        private List<Operation> _collectionOperation = new List<Operation>();

        public string NumCompte { get => _numCompte;}
        internal List<Operation> CollectionOperation { get => _collectionOperation;}
        internal Client Titulaire { get => _titulaire; set => _titulaire = value; }

        public Compte(string numCompte)
        {
            _numCompte = numCompte;

        }

        public Compte(string numCompte, Client client)
        {
            _numCompte = numCompte;
            _titulaire = client;
        }

        public void Crediter(float montant)
        {
            DateTime date = DateTime.Now;
            TypeOperation typeOperation = TypeOperation.CREDIT;
            Operation operation = new Operation(montant, date, typeOperation);
            _collectionOperation.Add(operation);
         
        }

        public void Debiter(float montant)
        {
            DateTime date = DateTime.Now;
            TypeOperation typeOperation = TypeOperation.DEBIT;
            Operation operation = new Operation(montant, date, typeOperation);
            _collectionOperation.Add(operation);

        }

        public float GetSolde()
        {
            float resultat = 0;
            TypeOperation typeOperation = TypeOperation.CREDIT;

            foreach (Operation collectionOperation in _collectionOperation)
            {
                if (typeOperation == collectionOperation.TypeOperation)
                {
                    resultat += collectionOperation.Montant;
                }
                else
                {
                    resultat -= collectionOperation.Montant;
                }
            }

            return resultat;
        }


    }
}
