﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace tpBanque
{
    class Program
    {
        static void Main(string[] args)
        {
            string numClient = "C1";
            string nomClient = "Durant";
            Client unClient = new Client(numClient, nomClient);

            Compte compte = new Compte("C.DURANT.1", unClient);


            compte.Crediter(50);
            compte.Debiter(25);

            Console.WriteLine("{0} : {1}", unClient.NomClient, compte.GetSolde());


            Console.Read();
        }

    }
}
