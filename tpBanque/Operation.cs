﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace tpBanque
{
    class Operation
    {
        private float _montant;
        private DateTime _date;
        private TypeOperation _typeOperation;

        public float Montant { get => _montant;  }
        public DateTime Date { get => _date; }
        public TypeOperation TypeOperation { get => _typeOperation; }

        public Operation(float montant, DateTime date, TypeOperation typeOperation)
        {
            if(montant < 0)
            {
                montant = montant * (-1);
                _montant = montant;
            }
            else
            {
                _montant = montant;
            }

            _date = date;
            _typeOperation = typeOperation;
        }



    }
}
